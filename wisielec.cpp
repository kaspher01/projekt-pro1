#include <iostream>
#include <fstream>
#include <random>
#include <algorithm>
#include <clocale>
#include <array>
#include <wchar.h>
#include <unistd.h>
#include <ncursesw/cursesw.h>


struct elem {
    int x;
    int y;
    char c;
};


elem wisielec[] ={
        {8,11,'_'},
        {9,11,'_'},
        {10,11,'_'},
        {11,11,'_'},
        {12,11,'_'},
        {13,11,'_'},
        {14,11,'_'},
        {15,11,'_'},
        {16,11,'_'},
        {17,11,'_'},
        {18,11,'_'},
        {19,11,'_'},
        {20,11,'_'},

        {14,10,'|'},
        {14,9,'|'},
        {14,8,'|'},
        {14,7,'|'},
        {14,6,'|'},

        {15,5,'_'},
        {16,5,'_'},
        {17,5,'_'},
        {18,5,'_'},
        {19,5,'_'},

        {19,6,'|'},
        {19,7,'O'},
        {19,8,'|'},
        {18,9,'/'},
        {20,9,'\\'},
        {18,8,'/'},
        {20,8,'\\'},





};

void draw(int start, int end) {
    for (int i = start; i <= end; i++) {
        mvaddch(wisielec[i].y, wisielec[i].x, wisielec[i].c);
    }
}

//Mechanizm gry
int printGuesses(int level, std::wstring randomWord) {

    auto const LEVEL = level;
    if(LEVEL == 0){
        mvprintw(5, 20, "Do zobaczenia później!");
        return 0;
    }
    std::wstring podkreslenia =  L"_______________________________________________";
    int numberOfChars = randomWord.length();
    std::wstring placeForGuesses = podkreslenia.substr(0,numberOfChars);
    int hangmanParts = 30;
    int lives = hangmanParts/LEVEL;
    int start = 0;
    wint_t c;
    // Comment to play normal hangman without answer
    //mvaddwstr(1, 3, randomWord.data());
    // ------
    mvaddwstr(3, 3, placeForGuesses.data());

    do {

        get_wch(&c);

        if(c == 'q'){
            move(3,30);
            printw("Do zobaczenia!");
            return 0;
        }

        bool found = false;

        for(int i = 0; i<numberOfChars; i++){
            if(randomWord[i] == c){
                placeForGuesses[i]=c;
                found = true;
            }
        }
        if(!found){
            draw(start, start + LEVEL);
            lives--;
            if(lives == 0){
                mvaddwstr(1, 3, randomWord.data());
                move(3,30);
                printw("Przegrana :( Naciśnij dowolny przycisk aby wyjść...");
                return 0;
            }
            start+=LEVEL;

        }

        mvaddwstr(3, 3, placeForGuesses.data());

        if( randomWord == placeForGuesses)
        {
            move(3,30);
            printw("Gratulacje! Naciśnij dowolny przycisk aby wyjść...");
            getch();
            return 1;
        }

    } while(c != 'q');

    return lives;
}
//Pobieranie wyrazu z pliku
std::wstring getWord(std::string fileName){
    // Generating random number
    std::mt19937 engine;
    std::uniform_int_distribution<int> distribution(0, 3056759);
    engine.seed(std::random_device{}());
    auto const RANDOM_NUMBER = distribution(engine);

    // Getting a word from file
    std::wifstream f(fileName);
    std::wstring word;
    std::locale loc("");
    f.imbue(loc);
    //f.open(fileName, std::ios::in);
    int current_line = 0;
    std::wstring star  = L"x";
    while(std::getline(f,word))
    {
        if(current_line == RANDOM_NUMBER) {
            break;
        }
        current_line++;
    }
    f.close();
    word.pop_back();
    return word;
}

int chooseLevel(){
    int level;
    bool chosen = false;
    mvprintw(3, 15, "Witaj, wybierz poziom rozgrywki.");
    do {
        mvprintw(4, 15, "Twój wybór:");
        mvprintw(5, 15, "(1) Łatwy - 15 szans na zgadnięcie");
        mvprintw(6, 15, "(2) Średni - 10 szans na zgadnięcie");
        mvprintw(7, 15, "(3) Trudny - 5 szans na zgadnięcie");
        mvprintw(8, 15, "(4) Ekspert - 2 szanse na zgadnięcie");
        mvprintw(9, 15, "(5) Wyjście");
        move(10,15);
        level = getch();
        switch (level) {
            case '1':
                chosen = true;
                level = 2;
                break;
            case '2':
                chosen = true;
                level = 3;
                break;
            case '3':
                chosen = true;
                level = 6;
                break;
            case '4':
                chosen = true;
                level = 15;
                break;
            case '5':
                chosen = true;
                level = 0;
                break;
            default:
                mvprintw(11, 15, "Nie poprawny poziom! Wybierz dobry numer 1-4 lub 5 aby wyjść");
                refresh();
                sleep(1);
                clear();
                break;
        }

    } while(chosen == false);

    clear();
    return level;
}

int main()
{

    setlocale(LC_ALL, "en_US.UTF-8");
    initscr();
    noecho();
    int level = chooseLevel();
    std::wstring word = getWord("slowa.txt");
    printGuesses(level, word);
    getch();
    endwin();
    return 0;
}


